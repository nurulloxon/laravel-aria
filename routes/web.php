<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('main',function (){
   return view('main.index');
});

Route::resource('about',\App\Http\Controllers\AboutController::class);

Route::resource('analysis',\App\Http\Controllers\AnalysisController::class);

Route::resource('call_me',\App\Http\Controllers\Call_meController::class);

Route::resource('contacts',\App\Http\Controllers\ContactsController::class);

Route::resource('customers',\App\Http\Controllers\CustomersController::class);

Route::resource('help_business',\App\Http\Controllers\Help_BusinessController::class);

Route::resource('home',\App\Http\Controllers\HomeController::class);

Route::resource('intro',\App\Http\Controllers\IntroController::class);

Route::resource('services',\App\Http\Controllers\ServicesController::class);

Route::resource('teams',\App\Http\Controllers\TeamsController::class);
