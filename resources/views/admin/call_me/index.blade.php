@extends('admin.layouts.app')

@section('main')


    <div class="table-responsive">
        <table class="table align-middle mb-0 w-100">
            <thead class="table-light">
            <tr>

                <th class="align-middle">ID</th>
                <th class="align-middle">Bold text</th>
                <th class="align-middle">Top text</th>
                <th class="align-middle">Option 1</th>
                <th class="align-middle">Option 2</th>
                <th class="align-middle">Option 3</th>
                <th class="align-middle">Name</th>
                <th class="align-middle">Phone</th>
                <th class="align-middle">Email</th>
                <th class="align-middle">Interested</th>
                <th class="align-middle">Chek text</th>
                <th class="align-middle">Action</th>
            </tr>
            </thead>
            <tbody>

            @foreach($call_me as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->bold_text}}</td>
                    <td>{{$item->top_text}}</td>
                    <td>{{$item->option_1}}</td>
                    <td>{{$item->option_2}}</td>
                    <td>{{$item->option_3}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->phone}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{$item->interested}}</td>
                    <td>{{$item->chek_text}}</td>
                    <td>
                        <form action="{{route('call_me.destroy',$item->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger p-1"><i class="dripicons-trash"></i></button>
                        </form>

                    </td>

                </tr>
            @endforeach


            </tbody>
        </table>
    </div>

@endsection


@section('create')


    <!-- Transaction Modal -->
    <div class="modal fade transaction-detailModal" tabindex="-1" role="dialog" aria-labelledby="transaction-detailModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="transaction-detailModalLabel">Malumotlar qo'shish</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <form action="{{route('call_me.store')}}" method="post">
                    @csrf
                    <div class="modal-body">

                        <label>Bold text</label>
                        <input type="text" name="bold_text" placeholder="Bold text" class="form-control" required>
                        <label>Top text</label>
                        <input type="text" name="top_text" placeholder="Top text" class="form-control" required>
                        <label>Option 1</label>
                        <input type="text" name="option_1" placeholder="Option1" class="form-control" required>
                        <label>Option 2</label>
                        <input type="text" name="option_2" placeholder="Option 2" class="form-control" required>
                        <label>Option 3</label>
                        <input type="text" name="option_3" placeholder="Option 3" class="form-control" required>
                        <label>Name</label>
                        <input type="text" name="name" placeholder="Name" class="form-control" required>
                        <label>Phone</label>
                        <input type="number" name="phone" placeholder="Phone" class="form-control" required>
                        <label>Email</label>
                        <input type="text" name="email" placeholder="Email" class="form-control" required>
                        <label>Interested</label>
                        <input type="text" name="interested" placeholder="Interested" class="form-control" required>
                        <label>Chek text</label>
                        <input type="text" name="chek_text" placeholder="Chek_text" class="form-control" required>


                    </div>


                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" data-bs-dismiss="modal">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end modal -->


@endsection
