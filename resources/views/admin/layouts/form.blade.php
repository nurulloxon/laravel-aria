
<!-- Transaction Modal -->
<div class="modal fade transaction-detailModal" tabindex="-1" role="dialog" aria-labelledby="transaction-detailModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="transaction-detailModalLabel">Malumotlar qo'shish</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <form action="{{route('about.store')}}" method="post">
                @csrf
                <div class="modal-body">

                    <label>Bold text</label>
                    <input type="text" name="bold_text" placeholder="Bold text" class="form-control" required value="{{}}">
                    <label>Top text</label>
                    <input type="text" name="top_text" placeholder="Top text" class="form-control" required>
                    <label>Option 1</label>
                    <input type="text" name="option_1" placeholder="Option1" class="form-control" required>
                    <label>Option 2</label>
                    <input type="text" name="option_2" placeholder="Option 2" class="form-control" required>
                    <label>Statistics</label>
                    <input type="text" name="statistics" placeholder="Statistics" class="form-control" required>
                    <label>Image</label>
                    <input type="text" name="image" placeholder="Image" class="form-control" required>

                </div>


                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" data-bs-dismiss="modal">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal -->
