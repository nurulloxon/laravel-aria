@extends('admin.layouts.app')

@section('main')

    <div class="table-responsive">
        <table class="table align-middle mb-0 w-100">
            <thead class="table-light">
            <tr>

                <th class="align-middle">ID</th>
                <th class="align-middle">Bold text</th>
                <th class="align-middle">Top text</th>
                <th class="align-middle">Name</th>
                <th class="align-middle">Job</th>
                <th class="align-middle">Image link</th>
                <th class="align-middle">Image</th>
                <th class="align-middle">Action</th>
            </tr>
            </thead>
            <tbody>

            @foreach($teams as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->bold_text}}</td>
                    <td>{{$item->top_text}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->job}}</td>
                    <td>{{$item->image_link}}</td>
                    <td>{{$item->image}}</td>
                    <td>
                        <form action="{{route('teams.destroy',$item->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger p-1"><i class="dripicons-trash"></i></button>
                        </form>

                    </td>
                </tr>
            @endforeach


            </tbody>
        </table>
    </div>
    <!-- end table-responsive -->
@endsection

@section('create')


    <!-- Transaction Modal -->
    <div class="modal fade transaction-detailModal" tabindex="-1" role="dialog" aria-labelledby="transaction-detailModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="transaction-detailModalLabel">Malumotlar qo'shish</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <form action="{{route('teams.store')}}" method="post">
                    @csrf
                    <div class="modal-body">

                        <label>Bold text</label>
                        <input type="text" name="bold_text" placeholder="Bold text" class="form-control" required>
                        <label>Top text</label>
                        <input type="text" name="top_text" placeholder="Top text" class="form-control" required>
                        <label>Name</label>
                        <input type="text" name="name" placeholder="Name" class="form-control" required>

                        <label>Job</label>
                        <input type="text" name="job" placeholder="Job" class="form-control" required>

                        <label>Image link</label>
                        <input type="text" name="image_link" placeholder="Image link" class="form-control" required>
                        <label>Image</label>
                        <input type="text" name="image" placeholder="Image" class="form-control" required>

                    </div>


                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" data-bs-dismiss="modal">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end modal -->


@endsection
