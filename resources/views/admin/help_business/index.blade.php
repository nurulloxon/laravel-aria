@extends('admin.layouts.app')

@section('main')
    <div class="table-responsive">
        <table class="table align-middle mb-0 w-100">
            <thead class="table-light">
            <tr>

                <th class="align-middle">ID</th>
                <th class="align-middle">Bold text</th>
                <th class="align-middle">Option 1</th>
                <th class="align-middle">Option 2</th>
                <th class="align-middle">Option 3</th>
                <th class="align-middle">Image</th>
                <th class="align-middle">Action</th>
            </tr>
            </thead>
            <tbody>

            @foreach($help_business as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->bold_text}}</td>
                    <td>{{$item->option_1}}</td>
                    <td>{{$item->option_2}}</td>
                    <td>{{$item->option_3}}</td>
                    <td>{{$item->image}}</td>

                    <td>
                        <form action="{{route('help_business.destroy',$item->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger p-1"><i class="dripicons-trash"></i></button>
                        </form>

                    </td>

                </tr>
            @endforeach


            </tbody>
        </table>
    </div>
    <!-- end table-responsive -->
@endsection



@section('create')


    <!-- Transaction Modal -->
    <div class="modal fade transaction-detailModal" tabindex="-1" role="dialog" aria-labelledby="transaction-detailModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="transaction-detailModalLabel">Malumotlar qo'shish</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <form action="{{route('help_business.store')}}" method="post">
                    @csrf
                    <div class="modal-body">

                        <label>Bold text</label>
                        <input type="text" name="bold_text" placeholder="Bold text" class="form-control" required>
                        <label>Option 1</label>
                        <input type="text" name="option_1" placeholder="Option" class="form-control" required>
                        <label> Option 2</label>
                        <input type="text" name="option_2" placeholder="Option 2" class="form-control" required>
                        <label>Option 3</label>
                        <input type="text" name="option_3" placeholder="Option 3" class="form-control" required>

                        <label>Image</label>
                        <input type="text" name="image" placeholder="Image" class="form-control" required>

                    </div>


                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" data-bs-dismiss="modal">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end modal -->


@endsection
