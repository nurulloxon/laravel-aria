@extends('admin.layouts.app')

@section('main')


    <div class="table-responsive">
        <table class="table align-middle mb-0 w-100">
            <thead class="table-light">
            <tr>

                <th class="align-middle">ID</th>
                <th class="align-middle">Bold text</th>
                <th class="align-middle">Top text</th>
                <th class="align-middle">Location</th>
                <th class="align-middle">Phone 1</th>
                <th class="align-middle">Phone 2</th>
                <th class="align-middle">Email link</th>
                <th class="align-middle">Bold text 2</th>
                <th class="align-middle">Image link</th>
                <th class="align-middle">Name</th>
                <th class="align-middle">Email</th>
                <th class="align-middle">Chek text</th>
                <th class="align-middle">Messenger</th>
                <th class="align-middle">Action</th>

            </tr>
            </thead>
            <tbody>

            @foreach($contacts as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->bold_text}}</td>
                    <td>{{$item->top_text}}</td>
                    <td>{{$item->location}}</td>
                    <td>{{$item->phone_1}}</td>
                    <td>{{$item->phone_2}}</td>
                    <td>{{$item->email_link}}</td>
                    <td>{{$item->bold_text_2}}</td>
                    <td>{{$item->image_link}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{$item->chek_text}}</td>
                    <td>{{$item->messenger}}</td>

                    <td>
                        <form action="{{route('contacts.destroy',$item->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger p-1"><i class="dripicons-trash"></i></button>
                        </form>

                    </td>
                </tr>
            @endforeach


            </tbody>
        </table>
    </div>

@endsection


@section('create')


    <!-- Transaction Modal -->
    <div class="modal fade transaction-detailModal" tabindex="-1" role="dialog" aria-labelledby="transaction-detailModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="transaction-detailModalLabel">Malumotlar qo'shish</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <form action="{{route('contacts.store')}}" method="post">
                    @csrf
                    <div class="modal-body">

                        <label>Bold text</label>
                        <input type="text" name="bold_text" placeholder="Bold text" class="form-control" required>

                        <label>Top text</label>
                        <input type="text" name="top_text" placeholder="Top text" class="form-control" required>

                        <label>Location</label>
                        <input type="text" name="location" placeholder="Location" class="form-control" required>
                        <label>Phone 1</label>
                        <input type="number" name="phone_1" placeholder="Phone 1" class="form-control" required>
                        <label>Phone 2</label>
                        <input type="number" name="phone_2" placeholder="Phone 2" class="form-control" required>
                        <label>Email Link</label>
                        <input type="text" name="email_link" placeholder="Email Link" class="form-control" required>
                        <label>Bold text 2</label>
                        <input type="text" name="bold_text_2" placeholder="Bolt text 2" class="form-control" required>
                        <label>Image link</label>
                        <input type="text" name="image_link" placeholder="Image link " class="form-control" required>
                        <label>Name</label>
                        <input type="text" name="name" placeholder="Email" class="form-control" required>
                        <label>Email</label>
                        <input type="text" name="email" placeholder="Email" class="form-control" required>
                        <label>Chek_text</label>
                        <input type="text" name="chek_text" placeholder="Chek text" class="form-control" required>
                        <label>Messenger </label>
                        <input type="text" name="messenger" placeholder="Messenger" class="form-control" required>


                    </div>


                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" data-bs-dismiss="modal">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end modal -->


@endsection


