@extends('admin.layouts.app')

@section('main')
    <h4 class="card-title mb-4">Latest Transaction</h4>
    <div class="table-responsive">
        <table class="table align-middle mb-0 w-100">
            <thead class="table-light">
            <tr>

                <th class="align-middle">ID</th>
                <th class="align-middle">Bold text</th>
                <th class="align-middle">Name text</th>
                <th class="align-middle">Top text</th>
                <th class="align-middle">Option 1</th>
                <th class="align-middle">Option 2</th>
                <th class="align-middle">Prices</th>
                <th class="align-middle">Image</th>
                <th class="align-middle">Action</th>
            </tr>
            </thead>
            <tbody>

            @foreach($services as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->bold_text}}</td>
                    <td>{{$item->top_text}}</td>
                    <td>{{$item->name_text}}</td>
                    <td>{{$item->option_1}}</td>
                    <td>{{$item->option_2}}</td>
                    <td>{{$item->prices}}</td>
                    <td>{{$item->image}}</td>
                    <td>
                        <form action="{{route('services.destroy',$item->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger p-1"><i class="dripicons-trash"></i></button>
                        </form>

                    </td>
                </tr>
            @endforeach


            </tbody>
        </table>
    </div>
    <!-- end table-responsive -->
@endsection


@section('create')


    <!-- Transaction Modal -->
    <div class="modal fade transaction-detailModal" tabindex="-1" role="dialog" aria-labelledby="transaction-detailModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="transaction-detailModalLabel">Malumotlar qo'shish</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <form action="{{route('services.store')}}" method="post">
                    @csrf
                    <div class="modal-body">

                        <label>Bold text</label>
                        <input type="text" name="bold_text" placeholder="Bold text" class="form-control" required>
                        <label>Name text</label>
                        <input type="text" name="name_text" placeholder="Name text" class="form-control" required>
                        <label>Top text</label>
                        <input type="text" name="top_text" placeholder="Top text" class="form-control" required>

                        <label>Option 1</label>
                        <input type="text" name="option_1" placeholder="Option 1" class="form-control" required>

                        <label>Option 2</label>
                        <input type="text" name="option_2" placeholder="Option 2" class="form-control" required>

                        <label>Prices</label>
                        <input type="text" name="prices" placeholder="Prices" class="form-control" required>
                        <label>Image</label>
                        <input type="text" name="image" placeholder="Image" class="form-control" required>

                    </div>


                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" data-bs-dismiss="modal">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end modal -->


@endsection
