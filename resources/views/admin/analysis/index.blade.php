@extends('admin.layouts.app')
@section('main')

    <div class="table-responsive">
        <table class="table align-middle mb-0 w-100">
            <thead class="table-light">
            <tr>

                <th class="align-middle">ID</th>
                <th class="align-middle">Bold text</th>
                <th class="align-middle">Top text</th>
                <th class="align-middle">Image</th>
                <th class="align-middle">Action</th>

            </tr>
            </thead>
            <tbody>

            @foreach($analysis as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->bold_text}}</td>
                    <td>{{$item->top_text}}</td>
                    <td>{{$item->image}}</td>
                    <td class="d-inline-flex">
                        <form action="{{route('analysis.destroy',$item->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger p-1"><i class="dripicons-trash"></i></button>
                        </form>

                        <button type="button" class="p-2 btn btn-success  waves-effect waves-light ms-3 mb-3 bx bxs-edit" data-bs-toggle="modal" data-bs-target="#update{{$item->id}}">

                        </button>


                        <!-- Transaction Modal -->
                        <div id="update{{$item->id}}" class="modal fade transaction-detailModal" tabindex="-1" role="dialog" aria-labelledby="transaction-detailModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="transaction-detailModalLabel">Malumotlar qo'shish</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>

                                    <form action="{{route('analysis.update',$item->id)}}" method="post">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">

                                            <label>Bold text</label>
                                            <input type="text" name="bold_text" placeholder="Bold text" class="form-control" required value="{{$item->bold_text}}">
                                            <label>Top text</label>
                                            <input type="text" name="top_text" placeholder="Top text" class="form-control" required value="{{$item->top_text}}">
                                            <label>Image</label>
                                            <input type="text" name="image" placeholder="Image" class="form-control" required value="{{$item->image}}">

                                        </div>


                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary" data-bs-dismiss="modal">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- end modal -->


                    </td>

                </tr>
            @endforeach


            </tbody>
        </table>
    </div>

@endsection


@section('create')


    <!-- Transaction Modal -->
    <div class="modal fade transaction-detailModal" tabindex="-1" role="dialog" aria-labelledby="transaction-detailModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="transaction-detailModalLabel">Malumotlar qo'shish</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <form action="{{route('analysis.store')}}" method="post">
                    @csrf
                    <div class="modal-body">

                        <label>Bold text</label>
                        <input type="text" name="bold_text" placeholder="Bold text" class="form-control" required>
                        <label>Top text</label>
                        <input type="text" name="top_text" placeholder="Top text" class="form-control" required>
                        <label>Image</label>
                        <input type="text" name="image" placeholder="Image" class="form-control" required>

                    </div>


                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" data-bs-dismiss="modal">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end modal -->


@endsection
