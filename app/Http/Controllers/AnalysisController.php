<?php

namespace App\Http\Controllers;

use App\Models\Analysis;
use Illuminate\Http\Request;

class AnalysisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $analysis=Analysis::all();
        return view('admin.analysis.index',compact('analysis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'bold_text'=>'required',
            'top_text'=>'required',

            'image'=>'required'

        ]);

        Analysis::create([
            'bold_text'=>$request->bold_text,
            'top_text'=>$request->top_text,

            'image'=>$request->image

        ]);



        return redirect()->route('analysis.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'bold_text'=>'required',
            'top_text'=>'required',

            'image'=>'required'

        ]);

        $analysis=Analysis::find($id);
        $analysis->bold_text=$request->bold_text;
        $analysis->top_text=$request->top_text;
        $analysis-> image=$request->image;
        $analysis->update();




        return redirect()->route('analysis.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $analysis=Analysis::find($id);
        $analysis->delete();
        return redirect()->route('analysis.index');
    }
}
