<?php

namespace App\Http\Controllers;

use App\Models\Contacts;
use Illuminate\Http\Request;

class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts=Contacts::all();
        return view('admin.contacts.index',compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'bold_text'=>'required',
            'top_text'=>'required',
            'location'=>'required',
            'phone_1'=>'required',
            'phone_2'=>'required',
            'email_link'=>'required',
            'bold_text_2'=>'required',
            'image_link'=>'required',
            'name'=>'required',

            'email'=>'required',


            'chek_text'=>'required',
            'messenger'=>'required'

        ]);

        Contacts::create([
            'bold_text'=>$request->bold_text,
            'top_text'=>$request->top_text,
            'location'=>$request->location,
            'phone_1'=>$request->phone_1,
            'phone_2'=>$request->phone_2,
            'email_link'=>$request->email_link,
            'bold_text_2'=>$request->bold_text_2,
            'image_link'=>$request->image_link,
            'name'=>$request->name,
            'email'=>$request->email,

            'chek_text'=>$request->chek_text,

            'messenger'=>$request->messenger



        ]);



        return redirect()->route('contacts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $contacts=Contacts::find($id);
        $contacts->delete();
        return redirect()->route('contacts.index');
    }
}
