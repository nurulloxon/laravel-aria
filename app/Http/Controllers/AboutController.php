<?php

namespace App\Http\Controllers;

use App\Models\About;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $about=About::all();

        return view('admin.about.index',compact('about'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'bold_text'=>'required',
             'top_text'=>'required',
            'option_1'=>'required',
            'option_2'=>'required',
            'statistics'=>'required',
            'image'=>'required'

        ]);

        About::create([
            'bold_text'=>$request->bold_text,
            'top_text'=>$request->top_text,
            'option_1'=>$request->option_1,
            'option_2'=>$request->option_2,
            'statistics'=>$request->statistics,
            'image'=>$request->image

        ]);



        return redirect()->route('about.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(About $about)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'bold_text'=>'required',
            'top_text'=>'required',
            'option_1'=>'required',
            'option_2'=>'required',
            'statistics'=>'required',
            'image'=>'required'

        ]);

        $about=About::find($id);
        $about->bold_text=$request->bold_text;
        $about-> top_text=$request->top_text;
        $about-> option_1=$request->option_1;
        $about->option_2=$request->option_2;
        $about->statistics=$request->statistics;
        $about-> image=$request->image;
        $about->update();


        return redirect()->route('about.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(About $about)
    {
        $about->delete();
        return redirect()->route('about.index');
    }
}
