<?php

namespace App\Http\Controllers;

use App\Models\Call_me;
use Illuminate\Http\Request;

class Call_meController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $call_me=Call_me::all();
        return view('admin.call_me.index',compact('call_me'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'bold_text'=>'required',
            'top_text'=>'required',
            'option_1'=>'required',
            'option_2'=>'required',
            'option_3'=>'required',
            'name'=>'required',
            'phone'=>'required',
            'email'=>'required',
            'interested'=>'required',
            'chek_text'=>'required'

        ]);

        Call_me::create([
            'bold_text'=>$request->bold_text,
            'top_text'=>$request->top_text,
            'option_1'=>$request->option_1,
            'option_2'=>$request->option_2,
            'option_3'=>$request->option_3,
            'name'=>$request->name,
            'phone'=>$request->phone,
            'email'=>$request->email,
            'interested'=>$request->interested,
            'chek_text'=>$request->chek_text


        ]);



        return redirect()->route('call_me.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Call_me $call_me)
    {
        $call_me->delete();
        return redirect()->route('call_me.index');
    }
}
