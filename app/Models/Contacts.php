<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    use HasFactory;
    protected $table='contacts';
    protected $fillable=['bold_text','top_text','location','phone_1','phone_2','email_link','bold_text_2','image_link','name','email','messenger','chek_text'];
    public $timestamps=false;
}
