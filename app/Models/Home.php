<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    use HasFactory;
    protected $table='home';
    protected $fillable=['carousel_text','top_text','image'];
    public $timestamps=false;

}
