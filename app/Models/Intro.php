<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Intro extends Model
{
    use HasFactory;
    protected $table='intro';
    protected $fillable=['bold_text','top_text','name_text','image'];
    public $timestamps=false;

}
