<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    use HasFactory;
    protected $table='about';
    protected $fillable=['bold_text','top_text','option_1','option_2','statistics','image'];
    public $timestamps=false;
}
