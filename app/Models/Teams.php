<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teams extends Model
{
    use HasFactory;
    protected $table='teams';
    protected $fillable=['bold_text','top_text','name','job','image_link','image'];
    public $timestamps=false;
}
