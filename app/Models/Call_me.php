<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Call_me extends Model
{
    use HasFactory;
    protected $table='call_me';
    protected $fillable=['bold_text','top_text','option_1','option_2','option_3','name','phone','email','interested','chek_text'];
    public $timestamps=false;
}
