<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    use HasFactory;
    protected $table='services';
    protected $fillable=['bold_text','name_text','top_text','option_1','option_2','prices','image'];
    public $timestamps=false;
}
