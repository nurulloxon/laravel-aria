<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Help_Business extends Model
{
    use HasFactory;
    protected $table='help_business';
    protected $fillable=['bold_text','option_1','option_2','option_3','image'];
    public $timestamps=false;
}
